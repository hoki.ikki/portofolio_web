import hero from "../../assets/hero.png"

export const projects = [
    {
        title:"Online Cinema",
        subtitle:"Reactjs and Golang",
        description:"Streaming apps for watching movie from anywhere",
        image:"https://res.cloudinary.com/dfebjhjpu/image/upload/v1671466176/waysbucks/hero_jnkter.png",
        link:"https://legendary-yeot-170403.netlify.app/",
    },
]